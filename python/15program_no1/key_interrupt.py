#! /usr/bin/env python
# coding: utf-8

#This program seems like C# event handler

class Event:

    def __init__(self):
        self.handlers = []

    def add(self, handler):
        self.handlers.append(handler)
        return self

    def remove(self, handler):
        self.handlers.remove(handler)
        return self

    def fire(self, sender, earg=None):
        for handler in self.handlers:
            handler(self, earg)

    __iadd__ = add # "e.add(handler)" equals to "e += handler"
    __isub__ = remove # "e.remove(handler)" equals to "e -= handler"
    __call__ = fire # "e.fire(earg)" equals to "e(earg)"

class Publisher:

    def __init__(self):
        self.evt_foo = Event()

    def foo(self):
        self.evt_foo(self)

def handle_foo(sender, earg):
    print "foo!"


if __name__ == "__main__":
    pub = Publisher()
    pub.evt_foo += handle_foo
    pub.foo()
