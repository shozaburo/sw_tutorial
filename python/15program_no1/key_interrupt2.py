#! /usr/bin/env python
# coding: utf-8

# This program utilizes os folk.

import os, sys

if __name__ == "__main__":

    pid = os.fork()

    if pid == 0:
        for i in range(1, 5):
            print "    child %s/4" % i
            for k in range(10000000):
                1
        print "    child end"
        sys.exit()

    else:
        for i in range(1, 4):
            print "parent %s/3" % i
            for k in range(4000000):
                1

        print "parent is waiting"
        os.wait()
        print "parent exit"
