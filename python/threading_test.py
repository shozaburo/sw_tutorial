#! /usr/bin/env python
# coding: utf-8

import threading, time, datetime

class TestThread(threading.Thread):
    def __init__(self, n, t):
        super(TestThread, self).__init__()
        self.n = n
        self.t = t
    def run(self):
        print "### start sub thread ###"
        #無限にLoopさせたいならfor文の代わりにWhile使えばいい
        for i in range(self.n):
            time.sleep(self.t)
            print "### sub thread : " + str(datetime.datetime.today())
        print "### end sub thread ###"

if __name__ == "__main__":
    th_c1 = TestThread(5, 5)
    th_c1.start()
    #th_c1.setDaemon(True)
    time.sleep(1)

    print "### start main thread ###"
    while True:
        time.sleep(2)
        print "### main thread ###"
