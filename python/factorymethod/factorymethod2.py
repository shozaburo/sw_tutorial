class Pond(object):
    def __init__(self, number_animals):
        self.animals = []
        for i in xrange(number_animals):
            animal = self.new_animal("animal%d"%(i,))
            self.animals.append(animal)

    def simulate_one_day(self):
        for animal in self.animals:
            animal.eat()
            animal.speak()
            animal.sleep()

    def new_animal(self, name):
        raise NotImplementedError


class FrogPond(Pond):

    def new_animal(self, name):
        return Frog(name)

class DuckPond(Pond):

    def new_animal(self, name):
        return Duck(name)

class Animal(object):
    def __init__(self, name):
        self.name = name

    def eat(self):
        print("Duck(%s) is eating."%(self.name,))

    def speak(self):
        print("Duck(%s) is crying."%(self.name,))

    def sleep(self):
        print("Duck(%s) is sleeping"%(self.name,))

if __name__ == "__main__":
    pond1 = Pond(2)
    pond1.simulate_one_day()
