#!/usr/bin/env python
# coding:utf-8

class Pond(object):
    def __init__(self, number_ducks):
        self.ducks = []
        for i in xrange(number_ducks):
            self.ducks.append(Duck("Duck%d"%(i,)))

    def simulate_one_day(self):
        for duck in self.ducks:
            duck.eat()
            duck.speak()
            duck.sleep()

class Duck(object):
    def __init__(self, name):
        self.name = name

    def eat(self):
        print("Duck(%s) is eating."%(self.name,))

    def speak(self):
        print("Duck(%s) is crying."%(self.name,))

    def sleep(self):
        print("Duck(%s) is sleeping"%(self.name,))

if __name__ == "__main__":
    pond1 = Pond(2)
    pond1.simulate_one_day()
