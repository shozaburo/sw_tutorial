#! /usr/bin/env python
# coding: utf-8

#This is an example that the number changes decimal to binary
a = 5371

#aをひと桁ずつとりだす
number = []
for i in range (0, 4):
    number.append(a % 10)
    a /= 10

#取り出した桁を、ひとつずつ10進数->2進数変換する。
for j in range(len(number)):

    digits = []
    for k in range(4):
        n = number[j]
        print "number:" + str(n)
        digits.append((n >> k) & 1)
        print digits[k]
    print str(digits[3]) + str(digits[2]) + str(digits[1]) + str(digits[0])
