#!/usr/bin/env python
# coding:utf-8
# windowsのコマンドプロンプトはshift_jis以外は文字化けするらしい

import sys
import json
#import csv

#定数
MENU_REGISTER = 1
MENU_QUERY = 2
MENU_DELETE = 3
MENU_UPDATE = 4
MENU_QUIT = 5
MENU_DEBUG = 9

FILENAME = "meibo.json"

meibo = []

def do_register(meibo_):
    print "register menu"
    print "name?"
    print ">>",
    input_name = sys.stdin.readline().strip()

    print "Birthday? (YYYYMMDD)"
    print ">>",
    #Error処理を後で追加
    #yyyy年MM月DD日に変換して格納
    input_birthday = sys.stdin.readline().strip()

    print "Salary (unit:yen)"
    print ">>",
    #Error処理を後で追加
    input_salary = sys.stdin.readline().strip()

    if meibo_:
        sort_meibo(meibo_)
        id_ = findemptyid(meibo_)
    else:
        id_ = 0

    dic = {"name":input_name, "birthday":input_birthday, "salary":input_salary, "id":id_}
    meibo_.append(dic)

    print "debug msg: added meibo"
    print dic
    #for i in range(len(meibo_)):
    #    print meibo_[i]

    filewritejson(meibo_)

def do_display(meibo_):
    sort_meibo(meibo_)
    print "display meibo"
    print "-----------------------------"
    print "id name birthday salary"
    print "-----------------------------"
    for cnt, profile in enumerate(meibo_):
        print "{0[id]} {0[name]} {0[birthday]} {0[salary]}".format(profile)

def do_delete(meibo_):
    print "please input id you delete"
    do_display(meibo_)
    try:
        print ">>",
        num = int(sys.stdin.readline())
    except ValueError:
        print "please input number"
        return

    for i in range(len(meibo_)):
        if num == meibo_[i]['id']:
            print "delete id:%d" %(num)
            del meibo_[i]
            filewritejson(meibo_)
            return
    print "can't find the id"

def do_update():
    print "update"

def findemptyid(meibo_):
    #print ("debug msg: find empty id")
    for cnt, profile in enumerate(meibo_):
        #print ("debug msg: ")
        #print cnt
        #print profile.get("id")

        if cnt != profile.get("id"):
            id_ = cnt
            break
        elif cnt == len(meibo_) - 1:
            id_ = len(meibo_)
    return id_

def sort_meibo(meibo_):
    meibo_.sort(cmp=lambda x,y: cmp(x["id"], y["id"]))
    print ("sorted meibo")
    #print meibo_

def filereadjson():
    print "debug msg: json read"
    f = open(FILENAME, "r")
    jsondata = json.load(f)
    #meibo_ = json.dumps(jsondata, sort_keys = True, indent = 4)
    #print jsondata
    return jsondata

def filewritecsv(meibo_):
    print "debug msg: csv write"
    #f = open(FILENAME, "a", encoding="utf-8") #python3.0
    print meibo_
    f = open(FILENAME, "wb")
    csvWriter = csv.writer(f)
    csvWriter.writerow(meibo_)
    f.close()

def filewritejson(meibo_):
    print "debug msg: json write"
    #f = open(FILENAME, "a", encoding="utf-8") #python3.0
    #print meibo_
    f = open(FILENAME, "w")
    json.dump(meibo_, f, sort_keys=True, indent=4)

    f.close()


if __name__ == "__main__":

    meibo = filereadjson()

    while True:
        print "\n"
        print "-----------------------------"
        print "please input number"
        print "1. register"
        print "2. display meibo"
        print "3. delete"
        print "4. update"
        print "5. quit"
        print "9. debug"
        print "-----------------------------"

        try:
            print ">>",
            input_menu = int(sys.stdin.readline())
        except ValueError:
            print "please input number"
            continue

        #print input_menu + "is writen"
        if input_menu == MENU_REGISTER:
            do_register(meibo)
        elif input_menu == MENU_QUERY:
            do_display(meibo)
        elif input_menu == MENU_DELETE:
            do_delete(meibo)
        elif input_menu == MENU_UPDATE:
            do_update()
        elif input_menu == MENU_QUIT:
            break
        #elif input_menu ==7:
        #    meibo = filereadjson()
        #elif input_menu == 8:
        #    filewritejson(meibo)
        elif input_menu == MENU_DEBUG:
            findemptyid(meibo)
        else:
            print "error. please input correct number."
