#!/usr/bin/env python
# coding:utf-8

import pickle
import Tkinter

import syain_meibo2

MEIBOFILE = "meibo.dat"

class GuiApp(Tkinter.Frame):

    def createwidgedts(self):
        #listbox with scrollbar
        self.frame1 = Tkinter.Frame(self)
        frame = self.frame1
        self.listbox = Tkinter.Listbox(frame, height=10, width=30, selectmode=SINGLE, takefocus=1)
        self.yscroll = Tkinter.Scrollbar(frame, orient=VERTICAL)

        #Location
        self.listbox.grid(row=0, column=0, sticky=NS)
        self.yscroll.grid(row=0, column=1, sticky=NS)

        #
        self.yscroll.config(command=self.listbox.yview)
        self.listbox.config(yscrollcommand=self.yscroll.set)
        self.listbox.bind("<ButtonRelease-1>", self.selectitem)

        self.frame1.grid(row=0,column=0)

    def mainloop(self):
        while(True):
            pass

if __name__ == '__main__':
    root = Tkinter.Tk()
    app = GuiApp()
    app.mainloop()
    root.destroy()
