#!/usr/bin/env python
# coding: utf-8

import sys
import pickle
import datetime
import calendar

MENU_REGISTER = 1
MENU_QUERY = 2
MENU_DELETE = 3
MENU_UPDATE = 4
MENU_QUIT = 5
MENU_DEBUG = 9

FILENAME = "meibos.dat"

class Profile:

    def __init__(self, name, birthday, salary, id_):
        self.name = name
        self.birthday = birthday
        self.salary = salary
        self.id = id_

    def __repr__(self):
        return "{}, {}, {}, {}".format(self.id, self.name, self.birthday, self.salary)

class MeiboContainer:
    def __init__(self):
        self.meibos = []

    def __len__(self):
        return len(self.meibos)

    def sort(self):
        self.meibos.sort(key=lambda x : x.id)

    def findid(self):
        if self.meibos:
            for cnt, profile in enumerate(self.meibos):
                if cnt != profile.id:
                    id_ = cnt
                    break
                elif cnt == len(self.meibos) - 1:
                    id_ = len(self.meibos)
            return id_
        else:
            id_ = 0
            return id_

    def __add__(self, item):
        self.meibos.append(item)
        self.sort()

    def __iadd__(self, item):
        self.__add__(item)
        return self

    def __getitem__(self, idx):
        return self.meibos[idx]

    def __setitem__(self, idx, item):
        self.meibos[idx] = item
        self.sort()

    def __delitem__(self, idx):
        del self.meibos[idx]

    def __repr__(self):
        for i in range(len(self.meibos)):
            print self.meibos[i]

    def display(self):
        print "-----------------------------"
        print "id name birthday salary"
        print "-----------------------------"
        for i in range(len(self.meibos)):
            print self.meibos[i]

class Menu:
    thisyear = datetime.date.today().year

    def register(self):
        name = raw_input('name: ')

        while True:
            birthday = raw_input('birthday(YYYYMMDD): ')
            try:
                yyyy = int(birthday[0:4])
                mm = int(birthday[4:6])
                dd = int(birthday[6:8])
                date = datetime.date(yyyy, mm, dd)
            except ValueError:
                print "please input correct date!"
                continue

            birthday = str(date.year) + "年" + str(date.month) + "月" + str(date.day) + "日"
            break

        salary = raw_input('salary: ')
        profile = Profile(name, birthday, salary, self.meibos.findid())

        self.meibos += profile
        self.save()

    def display(self):
        self.meibos.display()

    def delete(self):
        idx = int(raw_input('id number: '))
        del self.meibos[idx]

    def update(self):
        pass

    def save(self):
        f = open(FILENAME, 'wb')
        pickle.dump(self.meibos, f)

    def load(self):
        try:
            f = open(FILENAME, 'rb')
            self.meibos = pickle.load(f)
        except IOError:
            #Object-ka
            self.meibos = MeiboContainer()


if __name__ == '__main__':

        menu = Menu()
        menu.load()

        while True:
            print "\n"
            print "-----------------------------"
            print "please input number"
            print "1. register"
            print "2. display meibo"
            print "3. delete"
            print "4. update"
            print "5. quit"
            print "9. debug"
            print "-----------------------------"

            try:
                input_menu = int(raw_input('>>>'))
            except ValueError:
                print "please input number"
                continue

            if input_menu == MENU_REGISTER:
                menu.register()

            elif input_menu == MENU_QUERY:
                menu.display()

            elif input_menu == MENU_DELETE:
                menu.delete()

            elif input_menu == MENU_UPDATE:
                menu.update()

            elif input_menu == MENU_QUIT:
                break

            elif input_menu == MENU_DEBUG:
                pass
            else:
                print "error. please input correct number."
