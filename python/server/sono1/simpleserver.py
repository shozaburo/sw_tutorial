#python3: http.server
import SimpleHTTPServer
import SocketServer

server_address = ("", 8000)
handler_class = SimpleHTTPServer.SimpleHTTPRequestHandler #handler
httpd = SocketServer.TCPServer(server_address, handler_class)
httpd.serve_forever()
